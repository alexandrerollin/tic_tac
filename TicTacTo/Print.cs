using BoardClass;
using PlayerLib;

namespace TicTacTo;

public class Print
{

    public static void PrintBoot()
    {
        Console.Clear();
        Console.WriteLine($"||============||");
        Console.WriteLine($"||TIC TAC TOE ||");
        Console.WriteLine($"||============||");
        Console.WriteLine($"||WANNA PLAY? ||");
        Console.WriteLine($"||============||");
        Console.WriteLine($"|| X | O | X  ||");
        Console.WriteLine($"|| ---------- ||");
        Console.WriteLine($"|| O | X | O  ||");
        Console.WriteLine($"|| ---------- ||");
        Console.WriteLine($"|| X | O | X  ||");
        Console.WriteLine($"||============||");
        Console.Write($"||START [Y/N] || ");
    }
    public static void PrintChoice()
    {
        Console.Clear();
        Console.WriteLine($"||============||");
        Console.WriteLine($"||TIC TAC TOE ||");
        Console.WriteLine($"||============||");
        Console.WriteLine($"||ENTER CHOICE||");
        Console.WriteLine($"||============||");
        Console.WriteLine($"|| X | O | X  ||");
        Console.WriteLine($"|| ---------- ||");
        Console.WriteLine($"|| O | X | O  ||");
        Console.WriteLine($"|| ---------- ||");
        Console.WriteLine($"|| X | O | X  ||");
        Console.WriteLine($"||============||");
        Console.Write($"||START [Y/N] || ");
    }
    public static void PrintStart(Board gb,Player player)
    {
        Console.Clear();
        Console.WriteLine($"||============||");
        Console.WriteLine($"||TIC TAC TOE ||");
        Console.WriteLine($"||============||");
        Console.WriteLine($"||{player.Name.ToUpper().PadRight(7,' ') + " TURN"}||");
        Console.WriteLine($"||============||");
        Console.WriteLine($"|| {gb.Value[0]} | {gb.Value[1]} | {gb.Value[2]}  ||");
        Console.WriteLine($"|| ---------- ||");
        Console.WriteLine($"|| {gb.Value[3]} | {gb.Value[4]} | {gb.Value[5]}  ||");
        Console.WriteLine($"|| ---------- ||");
        Console.WriteLine($"|| {gb.Value[6]} | {gb.Value[7]} | {gb.Value[8]}  ||");
        Console.WriteLine($"||============||");
        Console.Write($"||ENTER CASE# || ");
    }
    public static void PrintEndGame(Board gb,Player player)
    {
        Console.Clear();
        Console.WriteLine($"||============||");
        Console.WriteLine($"||TIC TAC TOE ||");
        Console.WriteLine($"||============||");
        Console.ForegroundColor = ConsoleColor.DarkRed;
        Console.WriteLine($"||{player.Name.ToUpper().PadRight(6,' ') + " WINS"}||");
        Console.ResetColor();
        Console.WriteLine($"||============||");
        Console.WriteLine($"|| {gb.Value[0]} | {gb.Value[1]} | {gb.Value[2]}  ||");
        Console.WriteLine($"|| ---------- ||");
        Console.WriteLine($"|| {gb.Value[3]} | {gb.Value[4]} | {gb.Value[5]}  ||");
        Console.WriteLine($"|| ---------- ||");
        Console.WriteLine($"|| {gb.Value[6]} | {gb.Value[7]} | {gb.Value[8]}  ||");
        Console.WriteLine($"||============||");
        Console.Write($"||AGAIN[Y/N]  || ");
        
    }
    public static void PrintEndDraw(Board gb)
    {
        Console.Clear();
        Console.WriteLine($"||============||");
        Console.WriteLine($"||TIC TAC TOE ||");
        Console.WriteLine($"||============||");
        Console.ForegroundColor = ConsoleColor.DarkRed;
        Console.WriteLine($"||   !DRAW!   ||");
        Console.ResetColor();
        Console.WriteLine($"||============||");
        Console.WriteLine($"|| {gb.Value[0]} | {gb.Value[1]} | {gb.Value[2]}  ||");
        Console.WriteLine($"|| ---------- ||");
        Console.WriteLine($"|| {gb.Value[3]} | {gb.Value[4]} | {gb.Value[5]}  ||");
        Console.WriteLine($"|| ---------- ||");
        Console.WriteLine($"|| {gb.Value[6]} | {gb.Value[7]} | {gb.Value[8]}  ||");
        Console.WriteLine($"||============||");
        Console.Write($"||AGAIN[Y/N]  || ");
    }
    
}