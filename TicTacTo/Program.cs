﻿using static TicTacTo.Print;
using PlayerLib;
using BoardClass;
using static TicTacTo.Game;
namespace TicTacTo
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                
                Board gamingBoard = new Board();
                Player p1 = new Player("Player1");
                Player p2 = new Player("Player2");
                while (true)
                {
                    gamingBoard.ResetBoard();
                    int counter = 1;
                    PrintBoot();
                    if (GetYes())
                    {
                        int player1 = 1;
                        int player2 = 2;
                        while (IsOver(gamingBoard, counter) == 2)
                        {
                            if (counter % 2 != 0)
                            {
                                PrintStart(gamingBoard, p1);
                                PlayGame(PlayerChoice(),gamingBoard,player1);
                                counter++;
                            }
                            else
                            {
                                PrintStart(gamingBoard, p2);
                                PlayGame(PlayerChoice(),gamingBoard,player2);
                                counter++;
                            }
                        }
                        if (IsOver(gamingBoard, counter) == 0)
                        {
                            PrintEndDraw(gamingBoard);
                            GetYes();
                            break;
                        }

                        if (counter % 2 == 0)
                        {
                            PrintEndGame(gamingBoard, p1);
                            GetYes();
                        }
                        else
                        {
                            PrintEndGame(gamingBoard, p2);
                            GetYes();
                        }
                    }
                }
            }
        }
    }
}