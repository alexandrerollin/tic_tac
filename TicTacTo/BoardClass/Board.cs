﻿using System.Diagnostics.CodeAnalysis;

namespace BoardClass;

public class Board
{
    public char[] Value { get; private set; }

    public Board()
    {
        Value = new char[9]{'1','2','3','4','5','6','7','8','9',};
    }

    public void ResetBoard()
    {
        Value = new char[]{'1','2','3','4','5','6','7','8','9',};
    }

    public override bool Equals(object? obj)
    {
        Board other = (Board) obj;
        if (Value.Equals(other.Value))
        {
            return true;
        }

        return false;
    }
}