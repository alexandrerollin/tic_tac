using System.Collections.Specialized;
using BoardClass;
using PlayerLib;
using static TicTacTo.Print;

namespace TicTacTo;

public class Game
{
    public static void PlayGame(int index, Board gb,int player)
    {
        
        if (gb.Value[index] != 'X' && gb.Value[index] != 'O')
        {
            if (player % 2 != 0)
            {
                gb.Value[index] = 'X';
            }
            if (player % 2 == 0)
            {
                gb.Value[index] = 'O';
            }
        }
        else // if index already taken
        {
            Console.WriteLine("INVALID CHOICE");
            Thread.Sleep(1000);
            Console.Write("ENTER NEW CHOICE : ");
            index = PlayerChoice();
            PlayGame(index, gb, player);
        }
    }
    public static int IsOver(Board gb,int counter)
    {
        //horizontal
        if (gb.Value[0].Equals(gb.Value[1]) && gb.Value[0].Equals(gb.Value[2]))
        {
            return 1;
        }
        if (gb.Value[3].Equals(gb.Value[4]) && gb.Value[3].Equals(gb.Value[5]))
        {
            return 1;
        }
        if (gb.Value[6].Equals(gb.Value[7]) && gb.Value[6].Equals(gb.Value[8]))
        {
            return 1;
        }
        //vertical
        if (gb.Value[0].Equals(gb.Value[3]) && gb.Value[0].Equals(gb.Value[6]))
        {
            return 1;
        }
        if (gb.Value[1].Equals(gb.Value[4]) && gb.Value[1].Equals(gb.Value[7]))
        {
            return 1;
        }
        if (gb.Value[2].Equals(gb.Value[5]) && gb.Value[2].Equals(gb.Value[8]))
        {
            return 1;
        }
        //diagonal
        if (gb.Value[0].Equals(gb.Value[4]) && gb.Value[0].Equals(gb.Value[8]))
        {
            return 1;
        }
        if (gb.Value[2].Equals(gb.Value[4]) && gb.Value[2].Equals(gb.Value[6]))
        {
            return 1;
        }
        //condition for draw
        if (counter == 10)
        {
            return 0;
        }
        return 2;
    }
    public static bool GetYes()
    {
        char answer = char.Parse(Console.ReadLine().ToUpper());
        if (answer == 'Y')
        {
            return true;
        }
        return false;
    }
    public static int PlayerChoice()
    {
        int answer = int.Parse(Console.ReadLine());
        return answer - 1;
    }
}