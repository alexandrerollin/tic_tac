﻿namespace PlayerLib;

public class Player
{
    public static int NextId { get; private set; } = 1;
    public string Name { get; set; }
    public int Score { get; set; }
    public int PlayerId { get; }

    public Player(string name)
    {
        Name = name;
        Score = 0;
        PlayerId = NextId;
        NextId++;
    }
}